import os
import maya.cmds as cmds

from OWMImporter import read_owmat

textureList = {}
TexErrors = {}

def getRealPath(root,texture,inPath):
	global textureList, TexErrors
	outPath = inPath
	doPass = False
	
	if not os.path.isabs(inPath):
		#print ("normalizing real path")
		inPath = os.path.normpath('%s/%s' % (root, inPath))
	
	if os.path.isfile("%s.psd"%inPath):
		outPath = "%s.psd"%inPath
		#print "Found PSD for %s"%texture
	elif os.path.isfile("%s.tif"%inPath):
		outPath = "%s.tif"%inPath
		#print "Found TIF for %s"%texture
	elif os.path.isfile("%s.tga"%inPath):
		outPath = "%s.tga"%inPath
		#print "Found TGA for %s"%texture
	elif os.path.isfile("%s.png"%inPath):
		outPath = "%s.png"%inPath
		#print "Found PNG for %s"%texture
	else:
		if not texture in TexErrors:
			cmds.warning("Unable to find compatible image for %s. Please convert to TIF, TGA, PSD or PNG."%texture)
			outPath = "%s.tif"%inPath
			TexErrors[texture] = inPath
		doPass = True
	
	return outPath, doPass

def buildCollision(mname):
	#print "Building Collision shader..."
	
	#Build initial network
	shader=cmds.shadingNode("lambert",asShader=True,name=mname)
	shadinggroup=cmds.sets(renderable=True,empty=True,name="%sSG"%shader)
	cmds.connectAttr('%s.outColor' %shader, '%s.surfaceShader' % shadinggroup, force=True)
	
	checkerNode = cmds.shadingNode("checker", at=True, name="collisionChecker")
	p2dTex = cmds.shadingNode("place2dTexture", au=True)
	cmds.connectAttr('%s.outUV' %p2dTex, '%s.uv' % checkerNode)
	cmds.connectAttr('%s.outUvFilterSize' %p2dTex, '%s.uvFilterSize' % checkerNode)
	
	cmds.setAttr('%s.color1' %checkerNode, 1, 0, 1, type="double3")
	cmds.setAttr('%s.color2' %checkerNode, 0, 1, 0.25, type="double3")
	cmds.setAttr('%s.repeatU' %p2dTex, 50)
	cmds.setAttr('%s.repeatV' %p2dTex, 50)

	cmds.connectAttr('%s.outColor' %checkerNode, '%s.color' % shader)
	
	#Return the name of the finished shader
	return shadinggroup


	
# Lambert is bring used for materials with 2 or less textures. Assuming the 2 textures are Color and Normal.
def buildLambert(root, mname, material):
	global textureList, TexErrors
	#print "Building Lambert shader..."
	
	#Build initial network
	shader=cmds.shadingNode("lambert",asShader=True,name=mname)
	shadinggroup=cmds.sets(renderable=True,empty=True,name="%sSG"%shader)
	cmds.connectAttr('%s.outColor' %shader, '%s.surfaceShader' % shadinggroup, force=True)
	
	# Add and Assign Textures
	slot = 0	#Using a slot system until the OWMat file tells us which slot we should assign each texture to.  This at least makes sure all the proper textures are attached to this material...
	
	for texturetype in material.textures:
		typ = texturetype[1]
		texture = texturetype[0]
		realpath, doPass = getRealPath(root,texture,os.path.splitext(texture)[0])
		if (doPass == True):
			pass
		
		if texture in TexErrors:
			#print "// Warning: Unable to find compatible image for %s. Please convert to TIF, TGA, PSD or PNG."%texture
			realpath = "%s.tif"%realpath
			TexErrors[texture] = realpath

		try:
			#print ("final realpath: %s"% realpath)
			fn, fext = os.path.splitext(realpath)
			fpath, name = os.path.split(fn)
			finame = ("img_%s"%name)
			tex = None
			if fn in textureList:
				tex = textureList[fn]
			else:
				if not cmds.objExists(fn):
					file_node = cmds.shadingNode('file',name=finame,asTexture=True)
				else:
					file_node = finame

				textureList[fn] = file_node
				cmds.setAttr(("%s.fileTextureName" % file_node ),realpath,type="string")
				
				# We only need 2 slots for our Lambert shader
				if (slot == 0):
					cmds.connectAttr('%s.outColor' %file_node, '%s.color' % shader)
				elif (slot == 1):
					cmds.connectAttr('%s.outColor' %file_node, '%s.normalCamera' % shader)
				
				slot = slot +1
				
				textureUV = cmds.shadingNode('place2dTexture',name=("place2dTexture_%s"%name),asUtility=True)
				cmds.connectAttr(("%s.outUV" % textureUV), '%s.uvCoord' % file_node)
		except: pass
		
	#Return the name of the finished shader
	return shadinggroup
	
# Blinn is bring used for materials with 2 or less textures. Assuming the 2 textures are Color and Normal.
def buildBlinn(root, mname, material):
	global textureList, TexErrors
	#print "Building Blinn shader..."
	
	#Build initial network
	shader=cmds.shadingNode("blinn",asShader=True,name=mname)
	shadinggroup=cmds.sets(renderable=True,noSurfaceShader=True,empty=True,name="%sSG"%shader)
	cmds.connectAttr('%s.outColor' %shader, '%s.surfaceShader' % shadinggroup, force=True)
	
	# Add and Assign Textures
	slot = 0	#Using a slot system until the OWMat file tells us which slot we should assign each texture to.  This at least makes sure all the proper textures are attached to this material...
	
	for texturetype in material.textures:
		typ = texturetype[1]
		texture = texturetype[0]
		realpath, doPass = getRealPath(root,texture,os.path.splitext(texture)[0])
		if (doPass == True):
			pass
		
		try:
			#print ("final realpath: %s"% realpath)
			fn, fext = os.path.splitext(realpath)
			fpath, name = os.path.split(fn)
			finame = ("img_%s"%name)
			tex = None
			if fn in textureList:
				tex = textureList[fn]
			else:
				if not cmds.objExists(fn):
					file_node = cmds.shadingNode('file',name=finame,asTexture=True)
				else:
					file_node = finame

				textureList[fn] = file_node
				cmds.setAttr(("%s.fileTextureName" % file_node ),realpath,type="string")

				#This can currently support up to 8 textures
				if (slot == 0):
					cmds.connectAttr('%s.outColor' %file_node, '%s.color' % shader, f=True)
				elif (slot == 1):
					cmds.connectAttr('%s.outColor' %file_node, '%s.normalCamera' % shader)
				elif (slot == 2):
					cmds.connectAttr('%s.outColor' %file_node, '%s.specularColor' % shader)
				elif (slot == 3):
					cmds.connectAttr('%s.outColor.outColorR' %file_node, '%s.reflectivity' % shader)
				elif (slot == 4):
					cmds.connectAttr('%s.outColor.outColorR' %file_node, '%s.eccentricity' % shader)
				elif (slot == 5):
					cmds.connectAttr('%s.outColor' %file_node, '%s.transparency' % shader)
				elif (slot == 6):
					cmds.connectAttr('%s.outColor.outColorR' %file_node, '%s.diffuse' % shader)
				elif (slot == 7):
					cmds.connectAttr('%s.outColor' %file_node, '%s.reflectedColor' % shader)
				elif (slot == 8):
					cmds.connectAttr('%s.outColor.outColorR' %file_node, '%s.translucence' % shader)
				elif (slot == 9):
					cmds.connectAttr('%s.outColor.outColorR' %file_node, '%s.ambientColor.ambientColorR' % shader)
				elif (slot == 10):
					cmds.connectAttr('%s.outColor.outColorR' %file_node, '%s.ambientColor.ambientColorG' % shader)
				elif (slot == 11):
					cmds.connectAttr('%s.outColor.outColorR' %file_node, '%s.ambientColor.ambientColorB' % shader)
				elif (slot == 12):
					cmds.connectAttr('%s.outColor.outColorR' %file_node, '%s.incandescence.incandescenceR'% shader)
				elif (slot == 13):
					cmds.connectAttr('%s.outColor.outColorR' %file_node, '%s.incandescence.incandescenceG'% shader)
				elif (slot == 14):
					cmds.connectAttr('%s.outColor.outColorR' %file_node, '%s.incandescence.incandescenceB'% shader)
				else:
					print "// ERROR: There are too many connections! Connection %s = %s"%(slot, file_node)
				
				slot = slot +1
				
				textureUV = cmds.shadingNode('place2dTexture',name=("place2dTexture_%s"%name),asUtility=True)
				cmds.connectAttr(("%s.outUV" % textureUV), '%s.uvCoord' % file_node)
		except: pass
	
	#Return the name of the finished shader
	return shadinggroup
	
# The Stingray node is our ideal material (for now) At some point, I might build a Overwatch-specific shader network...
def buildStingray(root, mname, material):
	global textureList
	print "Building Stingray shader..."
	
	#Build initial network
	shader=cmds.shadingNode("StingrayPBS",asShader=True,name=mname)
	shadinggroup=cmds.sets(renderable=True,empty=True,name="%sSG"%shader)
	shaderfx=cmds.shaderfx(sfxnode=shader,initShaderAttributes=True)
	cmds.connectAttr('%s.outColor' %shader, '%s.surfaceShader' % shadinggroup, force=True)
	
	#Unless we get the texture, assume this is a lambert
	cmds.setAttr("%s.metallic"%shader, 0.0)
	cmds.setAttr("%s.roughness"%shader, 1.0)
	
	# Add and Assign Textures
	slot = 0	#Using a slot system until the OWMat file tells us which slot we should assign each texture to.  This at least makes sure all the proper textures are attached to this material...
	
	for texturetype in material.textures:
		typ = texturetype[1]
		texture = texturetype[0]
		realpath = os.path.splitext(texture)[0]+'.tga'
		if not os.path.isabs(realpath):
			print ("normalizing real path")
			realpath = os.path.normpath('%s/%s' % (root, realpath))
		
		#try:
		print ("final realpath: %s"% realpath)
		fn, fext = os.path.splitext(realpath)
		fpath, name = os.path.split(fn)
		finame = ("img_%s"%name)
		tex = None
		if fn in textureList:
			tex = textureList[fn]
		else:
			if not cmds.objExists(fn):
				file_node = cmds.shadingNode('file',name=finame,asTexture=True)
			else:
				file_node = finame

			textureList[fn] = file_node
			cmds.setAttr(("%s.fileTextureName" % file_node ),realpath,type="string")

			#This can currently support up to 8 textures
			if (slot == 0):
				cmds.setAttr("%s.use_color_map"%shader, 1)
				cmds.connectAttr('%s.outColor' %file_node, '%s.TEX_color_map' % shader)
			elif (slot == 1):
				cmds.setAttr("%s.use_normal_map"%shader, 1)
				cmds.connectAttr('%s.outColor' %file_node, '%s.TEX_normal_map' % shader)
			elif (slot == 2):
				cmds.setAttr("%s.use_metallic_map"%shader, 1)
				cmds.setAttr("%s.use_roughness_map"%shader, 1)
				cmds.connectAttr('%s.outColor.outColorR' %file_node, '%s.TEX_roughness_map' % shader)
				cmds.connectAttr('%s.outColor.outColorG' %file_node, '%s.TEX_metallic_map' % shader)
			elif (slot == 3):
				cmds.setAttr("%s.use_emissive_map"%shader, 1)
				cmds.connectAttr('%s.outColor' %file_node, '%s.TEX_emissive_map' % shader)
			elif (slot == 4):
				cmds.setAttr("%s.use_ao_map"%shader, 1)
				cmds.connectAttr('%s.outColor' %file_node, '%s.TEX_ao_map' % shader)
			elif (slot == 5):
				cmds.connectAttr('%s.outColor' %file_node, '%s.TEX_global_diffuse_cube' % shader)
			elif (slot == 6):
				cmds.connectAttr('%s.outColor' %file_node, '%s.TEX_global_specular_cube' % shader)
			elif (slot == 7):
				cmds.connectAttr('%s.outColor' %file_node, '%s.metallic' % shader)
			elif (slot == 8):
				cmds.connectAttr('%s.outColor' %file_node, '%s.roughness' % shader)
			else:
				cmds.connectAttr('%s.outColor' %file_node, '%s.base_color' % shader)
			
			slot = slot +1
			
			textureUV = cmds.shadingNode('place2dTexture',name=("place2dTexture_%s"%name),asUtility=True)
			cmds.connectAttr(("%s.outUV" % textureUV), '%s.uvCoord' % file_node)
		#except: pass
	
	#Return the name of the finished shader
	return shadinggroup

def read(filename, prefix = ''):
	global textureList
	textureList = {}
	TexErrors = {}
	#print("reading material file...")
	root, file = os.path.split(filename)
	data = read_owmat.read(filename)
	if not data: return None

	if data.header.major > 2:
		print("// Error: Specified OWMAT is too new, and is an incompatible version.")
		raise ImportError()
		return None
	elif data.header.major < 1 or (data.header.minor < 0 and data.header.minor > 1):
		print("// Error: Specified OWMAT is an incompatible version: %i.%i"%(data.header.major,data.header.minor))
		raise ImportError()
		return None
	
	m = {}

	#print ("Processing Materials from %s..."%file)
	for material in data.materials:
		mname = 'Mat_%s%016X' % (prefix, material.key)
		#print "Material Name: "+mname
		if cmds.objExists(mname):
			shader = mname
		else:
			#print("Building material %s..."%mname)
			#Need to create our own shaderFX network for Overwatch. Until then, we'll use these...
			if (cmds.about(version=True)>= 2016)and(1==0):	#disable for now, until we can fix it
				shader=buildStingray(root, mname, material)
			elif (material.textureCount > 2):
				shader=buildBlinn(root, mname, material)
			else:
				shader=buildLambert(root, mname, material)
				
		m[material.key] = shader
	return m, textureList
