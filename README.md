# OWMDL & OWMAP Importer for Maya #

This is a Python script for importing the Open Source OWMDL and OWMAP file formats. (Plus any other associated formats.)

This importer is being built and tested with Maya 2016. Other versions of Maya may work, but are not yet supported. If you experience an issue with a different version, please [open an issue](https://bitbucket.org/Kjasi/owmdl-maya-importer/issues?status=new&status=open) and include the version of Maya you are using.

## Installation ##
* [Download the repository](https://bitbucket.org/Kjasi/owmdl-maya-importer/downloads)
* Copy `OWMImporter.py`, and the `OWMImporter` directory into your `%UserProfile%\Documents\maya\2016\plug-ins` directory.
* Copy `OWMImporterOptions.mel` into your `%UserProfile%\Documents\maya\2016\scripts` directory.
* Open Maya, go to `File Import` and change the `Files of Type` to `Overwatch Importer`
* Select your Model or Map, and hit import.

## Troubleshooting ##
### Issue: You can't see `Overwatch Importer` in your `Files of Type` listing###
* Solution A: Go to Windows->Settings/Preferences->Plug-in Manager, and see if `OWMImporter.py` is listed, and has a checkbox in the Loaded box. If not, click the box.
* Solution B: Make sure you put the `OWMImporter.py` file into the `%UserProfile%\Documents\maya\2016\plug-ins` directory and NOT the `%UserProfile%\Documents\maya\2016\scripts` directory. Try Solution A again.
### Issue: You don't see any options when trying to import a file###
* Solution: Make sure you put the `OWMImporterOptions.mel` file into the `%UserProfile%\Documents\maya\2016\scripts` directory and NOT the `%UserProfile%\Documents\maya\2016\plug-ins` directory.

##Reporting an Issue##
If, after trying all of this, it still doesn't load, file an Issue, describing the problem, with which OS you are using, and what version of Maya you are trying it on. I will also need you to open your Script Editor, and copy all the lines starting with `// Overwatch Importer, v(Version)` and paste that into the Issue's description. This will help me see where the script broke.